# gk-bingo

A simple bingo command-line driven program.

Runs from the console. Just fire up the program, answer a series of questions and start the bingo fun!

Uses the java sdk with only test dependencies. Java source and target levels set to 1.8.

Have fun!
