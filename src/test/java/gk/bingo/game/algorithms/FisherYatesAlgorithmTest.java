package gk.bingo.game.algorithms;

import org.junit.Test;

import java.util.stream.IntStream;


public class FisherYatesAlgorithmTest {

    @Test
    public void testShuffle() {
        int[] array = IntStream.rangeClosed(0,1000).toArray();
        FisherYatesAlgorithm algorithm = new FisherYatesAlgorithm();
        algorithm.shuffle(array);
    }
}