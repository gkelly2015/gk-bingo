package gk.bingo.BingoGame;


public class BingoGameTest {
 /*
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testDefaultBuild() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Number of players must be a positive integer.");
        new BingoGame.Builder().build();
    }

    @Test
    public void testInvalidMaxNumberRange() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Max number range must be a positive integer.");
        new BingoGame.Builder().withNumberOfPlayers(1).withMaxNumberRange(null).build();
    }

    @Test
    public void testInvalidNumberOfPlayers() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Number of players must be a positive integer.");
        new BingoGame.Builder().withNumberOfPlayers(null).build();
    }

    @Test
    public void testInvalidNumberOfColumns() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Number of columns must be a positive integer.");
        new BingoGame.Builder().withNumberOfPlayers(1).withNumberOfColumns(null).build();
    }

    @Test
    public void testInvalidNumberOfRows() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Number of rows must be a positive integer.");
        new BingoGame.Builder().withNumberOfPlayers(1).withNumberOfRows(null).build();
    }

    @Test
    public void testInvalidNumberPerRow() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Numbers per row must be a positive integer.");
        new BingoGame.Builder().withNumberOfPlayers(1).withNumbersPerRow(null).build();
    }

    @Test
    public void testInvalidShuffleAlgorithm() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Shuffle algorithm cannot be null.");
        new BingoGame.Builder().withNumberOfPlayers(1).withShuffleAlgorithm(null).build();
    }

    @Test
    public void testInvalidConfiguration() {
        exceptionRule.expect(IllegalArgumentException.class);
        final Integer numberOfRows = 2;
        final Integer numbersPerRow = 1;
        final Integer maxNumberRange = 1;
        final String message = String.format("Insufficient number range specified " +
                "(%d) for a ticket with %d rows and %d numbers per row.", maxNumberRange, numberOfRows, numbersPerRow);
        exceptionRule.expectMessage(message);
        new BingoGame.Builder().withNumberOfPlayers(1).withNumberOfRows(numberOfRows).withNumbersPerRow(numbersPerRow).withMaxNumberRange(maxNumberRange).build();
    }

    @Test
    public void testValidConfiguration() {
        new BingoGame.Builder().withNumberOfPlayers(1).build();
    }
    */
}