package gk.bingo.game;

public class BingoNumberTest {
/*
    @Test
    public void testConstructor() {
        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        Integer expected = 1;
        assertEquals(expected, bingoNumber.getNumber());

        expected = 2;
        assertEquals(expected, bingoNumber.getRow());

        expected = 3;
        assertEquals(expected, bingoNumber.getColumn());
    }

    @Test
    public void testEquals() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 2, 3);

        assertEquals(bingoNumber, bingoNumber2);
    }

    @Test
    public void testEquals2() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 3, 4);

        assertEquals(bingoNumber, bingoNumber2);
    }

    @Test
    public void testNotEquals() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(2, 2, 3);

        assertNotEquals(bingoNumber, bingoNumber2);
    }

    @Test
    public void testHashCode() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 2, 3);

        assertEquals(bingoNumber.hashCode(), bingoNumber2.hashCode());
    }

    @Test
    public void testHashCode2() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 3, 4);

        assertEquals(bingoNumber.hashCode(), bingoNumber2.hashCode());
    }

    @Test
    public void testHashCodeNotEquals() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(2, 2, 3);

        assertNotEquals(bingoNumber.hashCode(), bingoNumber2.hashCode());
    }

    @Test
    public void testCompareToEquals() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 2, 3);

        assertEquals(0, bingoNumber.compareTo(bingoNumber2));
    }

    @Test
    public void testCompareToLessThan() {

        BingoNumber bingoNumber = new BingoNumber(1, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(2, 2, 3);

        assertEquals(-1, bingoNumber.compareTo(bingoNumber2));
    }

    @Test
    public void testCompareToGreaterThan() {

        BingoNumber bingoNumber = new BingoNumber(2, 2, 3);
        BingoNumber bingoNumber2 = new BingoNumber(1, 2, 3);

        assertEquals(1, bingoNumber.compareTo(bingoNumber2));
    }

    @Test
    public void testCompareToNull() {

        BingoNumber bingoNumber = new BingoNumber(2, 2, 3);

        assertEquals(1, bingoNumber.compareTo(null));
    }

    @Test
    public void testToString() {

        int number = 1;
        int row = 2;
        int column = 3;
        BingoNumber bingoNumber = new BingoNumber(number, row, column);
        String expected = "BingoNumber{" +
                "number=" + number +
                ", row=" + row +
                ", column=" + column +
                '}';

        assertEquals(expected, bingoNumber.toString());
    }
*/

}