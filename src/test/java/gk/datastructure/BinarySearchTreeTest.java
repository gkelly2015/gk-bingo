package gk.datastructure;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BinarySearchTreeTest {

    @Test
    public void testInsert() {

        final BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<>();
        bst.put(6, null);
        bst.put(4, null);
        bst.put(7, null);
        bst.put(1, null);
        bst.put(11, null);

        assertTrue(bst.isBST());
    }

    @Test
    public void testRetrieve() {
        final int[] testValues = new int[]{7,2,9,2,6,4,12,35,74,23,45,11};
        final BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<>();
        for (int testValue1 : testValues) {
            bst.put(testValue1, testValue1 + 1);
        }
        for (int testValue : testValues) {
            Integer expected = testValue + 1;
            Integer actual = bst.get(testValue);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void testRetrieveNotFound() {
        final int[] testValues = new int[]{7,2,9,2,6,4,12,35,74,23,45,11};
        final BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<>();
        for (int testValue : testValues) {
            bst.put(testValue, null);
        }

        assertNull(bst.get(33));
    }

}
