package gk.datastructure;

import java.util.Iterator;
import java.util.Stack;

/**
 * Binary Search Tree implementation.
 * @param <K> - K instance of a Comparable (should be immutable)
 * @param <V> - V value to be stored in the tree node.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BinarySearchTree<K extends Comparable<K>, V> {

    /**
     * Helper class representing the tree node.
     * @param <K> - K instance of a Comparable (should be immutable)
     * @param <V> - V value to be stored in the tree node.
     *
     * @since 1.0
     */
    static class Node<K, V> {
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Node)) return false;

            Node<?, ?> node = (Node<?, ?>) o;

            return key != null ? key.equals(node.key) : node.key == null;

        }

        @Override
        public int hashCode() {
            return key != null ? key.hashCode() : 0;
        }
    }

    private Node<K, V> root;

    public BinarySearchTree() {

    }

    public void put(final K key, final V value) {
        root = put(root, key, value);
    }

    private Node<K, V> put(final Node<K, V> node, final K key, final V value)  {

        /**
         * Base case, create and return the new node.
         */
        if (node == null) {
            return new Node<K, V>(key, value);
        }

        /**
         * Compare the key with the current node's key.
         */
        int compare = key.compareTo(node.key);
        if (compare < 0)
            // Recursive call to insert on the left leg of the tree.
            node.left = put(node.left, key, value);
        else if (compare > 0)
            // Recursive call to insert on the right leg of the tree.
            node.right = put(node.right, key, value);

        return node;
    }

    public V get(final K key) {
        Node<K, V> node = get(root, key);
        return node != null ? node.value : null;
    }

    private Node<K, V> get(final Node<K, V> root, final K key) {
        /**
         * Base Case
         * (1) The root is null
         * (2) The root's key equals our search key.
         */
        if (root==null || root.key.compareTo(key) == 0) {
            return root;
        }

        /**
         * Compare the key with the node key
         */
        if (key.compareTo(root.key) < 0) {
            return get(root.left, key);
        } else {
            return get(root.right, key);
        }
    }

    public boolean isBST() {
        return isBST(root, null, null);
    }

    private boolean isBST(Node<K, V> node, K min, K max) {
        if (node == null) return true;
        if (min != null && node.key.compareTo(min) <= 0) return false;
        if (max != null && node.key.compareTo(max) >= 0) return false;
        return isBST(node.left, min, node.key) && isBST(node.right, node.key, max);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinarySearchTree)) return false;

        BinarySearchTree<?, ?> that = (BinarySearchTree<?, ?>) o;

        return root != null ? root.equals(that.root) : that.root == null;

    }

    @Override
    public int hashCode() {
        return root != null ? root.hashCode() : 0;
    }

    public Iterator<K> keyIterator() {
        return new KeyIterator(root);
    }


    /**
     * Helper class representing the key {@link Iterator}.
     *
     * @since 1.0
     */
    class KeyIterator implements Iterator<K> {

        private Stack<Node<K,V>> stack = new Stack<>();
        private Node<K,V> current;

        KeyIterator(Node<K, V> root) {
            current = root;
        }

        public K next() {
            while (current != null) {
                stack.push(current);
                current = current.left;
            }

            current = stack.pop();
            Node<K,V> node = current;
            current = current.right;

            return node.key;
        }

        public boolean hasNext() {
            return (!stack.isEmpty() || current != null);
        }
    }
}
