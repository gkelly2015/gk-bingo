package gk.bingo;

import gk.bingo.game.BingoGame;
import gk.bingo.game.BingoGameBuilder;
import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.adjudicator.BingoAdjudicatorEnum;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.events.MessageBusEnum;
import gk.bingo.game.events.MessageBusFactory;
import gk.bingo.game.io.*;
import gk.bingo.game.player.Player;
import gk.bingo.game.player.PlayerEnum;
import gk.bingo.game.player.PlayerFactory;
import gk.patterns.AbstractFactory;

import java.util.Scanner;
import java.util.Stack;
import java.util.stream.IntStream;

/**
 * Entry point into the bingo game.
 * First the user is asked a series of questions in order to configure the game.
 * Next the bingo game is created, followed by the players. Finally, the players
 * are registered with the game, and the game commences.
 *
 * The user is prompted to enter N to ask the game to drawn the next number. The
 * user can enter Q at any time to exit the game.
 *
 * The caller will drawn the next number from the bingo machine. The caller announces
 * the next number to the participants, the players are responsible for checking off
 * the number if it appears on their game card, and watching for winning game strategies.
 *
 * If the player notices a winning strategy, a claim is initiated, and is adjudicated by
 * the adjudicator. If the adjudicator accepts the winning claim, the announcer anounces
 * the win to the bingo players. The winning game strategy is then non-eligible to be
 * reused throughout the remainder of the game. If the adjudicator rejects the claim,
 * the game continues unaffected, or the announcer announces the clain rejection, or
 * the adjudicator calls foul and immediately terminates the game, depending on the
 * adjudicator strategy in play.
 *
 * Once all of the game strategies have been claimed, the game ends and the announcer
 * announces the winners and losers.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class Application {

    private final static String EXIT_CHARACTER = "Q";
    private final static String NEXT_CHARACTER = "N";

    public static void main(String[] args) {

        System.out.println("*** Lets Play Housie ***\n");
        System.out.println("Note:- Press 'Q' to quit any time.\n");

        final AbstractFactory<MessageBusEnum, MessageBus> messageBusFactory = new MessageBusFactory();
        final MessageBus messageBus = messageBusFactory.create(MessageBusEnum.SIMPLE_MESSAGE_BUS);
        final AbstractFactory<PlayerEnum, Player> playerFactory = new PlayerFactory(messageBus);

        try (Scanner scanner = new Scanner(System.in)) {
            final BingoGameParameters.Builder builder = parseInput(scanner);
            final BingoGameParameters bingoGameParameters = builder.build();

            final BingoGame bingoGame = new BingoGameBuilder(messageBus, bingoGameParameters)
                    .withBingoAdjudicatorEnum(BingoAdjudicatorEnum.HIGH_STAKES_BINGO_ADJUDICATOR)
                    .build();
            final Integer numberOfPlayers = bingoGameParameters.getNumberOfPlayers();
            IntStream.rangeClosed(1, numberOfPlayers).forEach(i -> {
                final Player player = playerFactory.create(PlayerEnum.STANDARD_PLAYER);
                bingoGame.register(player);
            });
            //bingoGame.register(playerFactory.create(PlayerEnum.BAD_ACTOR));

            System.out.println("\n\n*** Ticket Created Successfully ***\n");
            System.out.println("Press 'N' to generate next number. \n");

            while (scanner.hasNextLine()) {
                final String input = scanner.nextLine();
                if (EXIT_CHARACTER.equalsIgnoreCase(input)) {
                    System.exit(0);
                } else if (NEXT_CHARACTER.equalsIgnoreCase(input)) {
                    bingoGame.callNextNumber();
                } else{
                    System.out.println("Invalid entry. Press 'N' to generate next number or 'Q' to quit. \n");
                }
            }
        }
    }


    private static BingoGameParameters.Builder  parseInput(final Scanner scanner) {

        BingoGameParameters.Builder builder = new BingoGameParameters.Builder();

        final Stack<InputParser> parsers = new Stack<>();
        parsers.push(new NumbersPerRowParser(builder));
        parsers.push(new TicketSizeParser(builder));
        parsers.push(new NumberOfPlayersParser(builder));
        parsers.push(new MaxNumberRangeParser(builder));

        do {
            final InputParser parser = parsers.peek();
            parser.prompt();
            boolean parsed = false;
            while (!parsed && scanner.hasNextLine()) {
                final String input = scanner.nextLine();
                if (EXIT_CHARACTER.equalsIgnoreCase(input)) {
                    System.exit(0);
                }
                parsed = parser.accept(input);
                if (parsed) {
                    parsers.pop();
                } else {
                    parser.prompt();
                }
            }
        } while (!parsers.isEmpty());

        return builder;
    }
}
