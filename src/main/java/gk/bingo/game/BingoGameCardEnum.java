package gk.bingo.game;

/**
 * Represents the catalog of BingoGameCardEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoGameCardEnum {
    OPTIMIZED_BINGO_GAME_CARD;
}
