package gk.bingo.game.machine;


import gk.patterns.Flyweight;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class IntegerCache implements Flyweight<Integer, Integer> {

    private final Map<Integer, Integer> integerCache;

    public IntegerCache() {
        this.integerCache = new HashMap<>(1000);
        IntStream.rangeClosed(1, 1000).forEach(i -> {
            integerCache.put(i, i);
        });
    }

    @Override
    public Integer get(Integer key) {
        Integer cached = integerCache.get(key);
        if (cached == null) {
            cached = create(key);
            integerCache.put(key, cached);
        }
        return cached;
    }

    @Override
    public Integer create(Integer key) {
        return null;
    }
}
