package gk.bingo.game.machine;

/**
 * Bingo number represents the number and also the co-ordinates it occupies in the bingo game card.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoNumber implements Comparable<BingoNumber> {

    private final Integer number;
    private final Integer row;
    private final Integer column;

    BingoNumber(final Integer number, final Integer row, final Integer column) {
        this.number = number;
        this.row = row;
        this.column = column;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getRow() {
        return row;
    }

    public Integer getColumn() {
        return column;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof BingoNumber)) return false;

        final BingoNumber that = (BingoNumber) o;
        return number.equals(that.number);
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }

    @Override
    public int compareTo(BingoNumber bingoNumber) {
        if (bingoNumber != null) {
            return getNumber().compareTo(bingoNumber.getNumber());
        }
        return 1;
    }

    @Override
    public String toString() {
        return "BingoNumber{" +
                "number=" + number +
                ", row=" + row +
                ", column=" + column +
                '}';
    }
}
