package gk.bingo.game.machine;

/**
 * Bingo machine has two distinct but related responsibilities.
 * When a player registers to play the game, the BingoMachine generates a game cards based on the
 * game parameters provided. The second responsibility is to allow the caller to request the next number
 * in the sequence to be announced to the world.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoMachine {

    /**
     * Generate the series of {@link BingoNumber} numbers that will make up the player's game card.
     * @return - BingoNumber[]
     */
    BingoNumber[] generateNumbers();

    /**
     * Draw the next number from the sequence of house numbers.
     * @return
     */
    Integer nextNumber();
}
