package gk.bingo.game.machine;

import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.algorithms.ShuffleAlgorithm;

import java.util.Stack;
import java.util.stream.IntStream;

/**
 * Standard {@link BingoMachine} implementation. It uses the shuffle algorithm provided to perform two tasks;
 * <ul>
 *     <li>Generate random bingo numbers for the game card</li>
 *     <li>Generate the series of numbers that will be drawn as part of the bingo game</li>
 * </ul>
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class StandardBingoMachine implements BingoMachine {

    private final BingoGameParameters bingoGameParameters;
    private final ShuffleAlgorithm shuffleAlgorithm;
    private final Integer minNumberRange;
    private final Integer maxNumberRange;
    private final Stack<Integer> stack;

    StandardBingoMachine(final BingoGameParameters bingoGameParameters) {
        this.bingoGameParameters = bingoGameParameters;
        minNumberRange = bingoGameParameters.getMinNumberRange();
        maxNumberRange = bingoGameParameters.getMaxNumberRange();
        shuffleAlgorithm = bingoGameParameters.getShuffleAlgorithm();
        final int[] numbers = IntStream.rangeClosed(minNumberRange, maxNumberRange).toArray();
        shuffleAlgorithm.shuffle(numbers);

        stack = new Stack<>();
        for (int n : numbers) {
            stack.push(n);
        }
    }

    @Override
    public BingoNumber[] generateNumbers() {
        final int[] numbers = IntStream.rangeClosed(minNumberRange, maxNumberRange).toArray();
        shuffleAlgorithm.shuffle(numbers);

        final Integer numbersPerRow = bingoGameParameters.getNumbersPerRow();
        final Integer bingoNumbersArrayLength = bingoGameParameters.getNumbersPerCard();
        final BingoNumber[] bingoNumbers = new BingoNumber[bingoNumbersArrayLength];
        int row = 1;
        int col = 1;
        for (int i=0; i<bingoNumbers.length; i++) {
            bingoNumbers[i] = new BingoNumber(numbers[i], row, col);
            if (numbersPerRow <= col) {
                row++;
                col = 1;
            } else {
                col++;
            }
        }
        return bingoNumbers;
    }

    @Override
    public Integer nextNumber() {
        return !stack.isEmpty() ? stack.pop() : null;
    }
}
