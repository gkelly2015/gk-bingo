package gk.bingo.game.machine;

import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.caller.BingoCaller;
import gk.bingo.game.caller.BingoCallerEnum;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoCaller}. All available concrete implementations are catalogued in the {@link BingoCallerEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoMachineFactory implements AbstractFactory<BingoMachineEnum, BingoMachine> {

    private final BingoGameParameters bingoGameParameters;

    public BingoMachineFactory(final BingoGameParameters bingoGameParameters) {
        this.bingoGameParameters = bingoGameParameters;
    }

    @Override
    public BingoMachine create(final BingoMachineEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("BingoNumbersGeneratorEnum cannot be null");

        switch (discriminator) {
            case STANDARD_BINGO_MACHINE:
                return new StandardBingoMachine(bingoGameParameters);
            default:
                throw new IllegalArgumentException("Unsupported BingoNumbersGeneratorEnum requested: " + discriminator);
        }
    }
}
