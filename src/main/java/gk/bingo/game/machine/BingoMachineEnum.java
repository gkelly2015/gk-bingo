package gk.bingo.game.machine;


/**
 * Represents the catalog of BingoMachineEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoMachineEnum {
    STANDARD_BINGO_MACHINE;
}
