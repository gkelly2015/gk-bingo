package gk.bingo.game.adjudicator;

import gk.bingo.game.events.MessageBus;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;
import gk.patterns.AbstractFactory;

import java.util.Map;


/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoAdjudicator}. All available concrete implementations are catalogued in the {@link BingoAdjudicatorEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoAdjudicatorFactory implements AbstractFactory<BingoAdjudicatorEnum, BingoAdjudicator> {

    private final MessageBus messageBus;
    private final Map<GameStrategyEnum, GameStrategy> gameStrategies;

    public BingoAdjudicatorFactory(final MessageBus messageBus, final Map<GameStrategyEnum, GameStrategy> gameStrategies) {
        this.messageBus = messageBus;
        this.gameStrategies = gameStrategies;
    }

    @Override
    public BingoAdjudicator create(BingoAdjudicatorEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("BingoAdjudicatorEnum cannot be null");

        switch (discriminator) {
            case STANDARD_BINGO_ADJUDICATOR:
                return new StandardBingoAdjudicator(messageBus, gameStrategies);
            case VOCAL_BINGO_ADJUDICATOR:
                return new VocalBingoAdjudicator(messageBus, gameStrategies);
            case HIGH_STAKES_BINGO_ADJUDICATOR:
                return new HighStakesBingoAdjudicator(messageBus, gameStrategies);
            default:
                throw new IllegalArgumentException("Unsupported BingoAdjudicatorEnum type received: " + discriminator);
        }
    }
}
