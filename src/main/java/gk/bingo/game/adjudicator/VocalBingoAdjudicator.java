package gk.bingo.game.adjudicator;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;

import java.util.Map;

/**
 * Standard implementation of a {@link BingoAdjudicator}. This implementation will enforce the following constraints
 * <ul>
 *     <li>The player making the claim has actually registered to play the game.</li>
 *     <li>The {@link GameStrategy} being claimed hasn't already been claimed by another player.</li>
 *     <li>The {@link BingoGameCard} being claimed satisfies the criteria of the {@link GameStrategy} being claimed for.</li>
 * </ul>
 * This particular instance will alert all interested parties that a claim has been rejected.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class VocalBingoAdjudicator extends StandardBingoAdjudicator {

    private final MessageBus messageBus;

    VocalBingoAdjudicator(final MessageBus messageBus, final Map<GameStrategyEnum, GameStrategy> gameStrategies) {
        super(messageBus, gameStrategies);
        this.messageBus = messageBus;
    }

    @Override
    protected void rejectClaim(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard, final String reason) {
        messageBus.rejectClaim(playerIdentity, gameStrategy, gameCard, reason);
    }
}
