package gk.bingo.game.adjudicator;

/**
 * Represents the catalog of BingoAdjudicators available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoAdjudicatorEnum {
    STANDARD_BINGO_ADJUDICATOR,
    VOCAL_BINGO_ADJUDICATOR,
    HIGH_STAKES_BINGO_ADJUDICATOR;
}
