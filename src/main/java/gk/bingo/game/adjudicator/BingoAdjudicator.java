package gk.bingo.game.adjudicator;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.NumberDrawnListener;
import gk.bingo.game.events.PlayerRegisteredListener;
import gk.bingo.game.events.WinningClaimListener;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

/**
 * BingoAdjudicator is the rules enforcer in the system.
 *
 * Concrete implementations are responsible for adjudicating claims from players with winning cards. Naturally
 * BingoAdjudicator implementations will require to listening to the following event listeners:
 * <ul>
 *     <li>{@link PlayerRegisteredListener}- to keep track of all registered players in the system.</li>
 *     <li>{@link NumberDrawnListener}- to keep track of the numbers drawn by the caller.</li>
 *     <li>{@link WinningClaimListener}- to be notified of a winning claim..</li>
 * </ul>
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoAdjudicator extends PlayerRegisteredListener, NumberDrawnListener, WinningClaimListener {

    /**
     * The player identified by the {@link PlayerIdentity} is claiming the {@link BingoGameCard} is a winning card
     * based on the {@link GameStrategy} provided. Implementors of this interface must enforce the rules to adjudicate
     * the claim. The claim will be either accepted or rejected.
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy  - GameStrategy
     * @param gameCard - BingoGameCard
     */
    void ajudicateClaim(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard);
}
