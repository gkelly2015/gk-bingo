package gk.bingo.game.adjudicator;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;

import java.awt.*;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Specialized implementation of a {@link BingoAdjudicator}. This implementation will enforce the following constraints
 * <ul>
 *     <li>The player making the claim has actually registered to play the game.</li>
 *     <li>The {@link GameStrategy} being claimed hasn't already been claimed by another player.</li>
 *     <li>The {@link BingoGameCard} being claimed satisfies the criteria of the {@link GameStrategy} being claimed for.</li>
 * </ul>
 * If a claim is made and rejected, this instance will sound the alarm (literally) when a violation is detected. In addition,
 * a warning message is printed and the game is immediately terminated.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class HighStakesBingoAdjudicator extends VocalBingoAdjudicator {

    private final MessageBus messageBus;

    HighStakesBingoAdjudicator(final MessageBus messageBus, final Map<GameStrategyEnum, GameStrategy> gameStrategies) {
        super(messageBus, gameStrategies);
        this.messageBus = messageBus;
    }

    @Override
    protected void rejectClaim(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard, String reason) {
        super.rejectClaim(playerIdentity, gameStrategy, gameCard, reason);
        ringAlarm(5);
        System.out.println("*************************************************************************************************");
        System.out.println("**                         F  R  A  U  D     D  E  T  E  C  T  E  D                            **");
        System.out.println("** This is the super secure bingo adjuster.                                                    **");
        System.out.println("** We have detected fraud in the system.                                                       **");
        System.out.println("** As a security precaution we are stopping this game to investigate the nature of the fraud.  **");
        System.out.println("** We have detected fraud in the system.                                                       **");
        System.out.println("*************************************************************************************************");
        messageBus.announceGameOver();
    }

    protected void ringAlarm(final Integer numberOfTimesToBeep) {
        IntStream.rangeClosed(1, numberOfTimesToBeep).forEach(i -> Toolkit.getDefaultToolkit().beep());
    }
}
