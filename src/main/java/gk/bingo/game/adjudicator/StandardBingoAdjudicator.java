package gk.bingo.game.adjudicator;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;
import gk.datastructure.BinarySearchTree;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Standard implementation of a {@link BingoAdjudicator}. This implementation will enforce the following constraints
 * <ul>
 *     <li>The player making the claim has actually registered to play the game.</li>
 *     <li>The {@link GameStrategy} being claimed hasn't already been claimed by another player.</li>
 *     <li>The {@link BingoGameCard} being claimed satisfies the criteria of the {@link GameStrategy} being claimed for.</li>
 * </ul>
 * This particular instance does nothing if any of the constraints above are not satisfied. It simply rejects the claim silently
 * alerting nobody, and allows the game to continue.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class StandardBingoAdjudicator implements BingoAdjudicator {

    private final MessageBus messageBus;
    private final Set<PlayerIdentity> players = new LinkedHashSet<>();
    private final BinarySearchTree<Integer, Integer> numbersDrawn = new BinarySearchTree<>();
    private final Map<GameStrategyEnum, GameStrategy> gameStrategies;

    StandardBingoAdjudicator(final MessageBus messageBus, final Map<GameStrategyEnum, GameStrategy> gameStrategies) {
        this.messageBus = messageBus;
        this.gameStrategies = gameStrategies;
        this.messageBus.registerNumberDrawnListener(this);
        this.messageBus.registerWinningClaimListener(this);
        this.messageBus.registerPlayerRegistrationListener(this);
    }

    @Override
    public void numberDrawnEvent(final Integer number) {
        if (number == null) return;

        numbersDrawn.put(number, number);
    }

    @Override
    public void ajudicateClaim(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard) {
        if (playerIdentity == null || gameStrategy == null || gameCard == null) return;

        // Ensure player is registered.
        if (!players.contains(playerIdentity)) {
            final String reason = String.format("Rejected winning claim from %s since player is not registered in this game.", playerIdentity);
            rejectClaim(playerIdentity, gameStrategy, gameCard, reason);
            return;
        }

        // Ensure game strategy hasn't already been won.
        if (!gameStrategies.containsKey(gameStrategy.getType())) {
            final String reason = String.format("Rejected winning claim from %s since game strategy %s has already been won!", playerIdentity, gameStrategy.getType());
            rejectClaim(playerIdentity, gameStrategy, gameCard, reason);
            return;
        }

        // Validate the game card.
        if (gameStrategy.validate(gameCard, numbersDrawn)) {
            // We are about to announce a win for this strategy.
            // Now we need to remove it to ensure it does't get claimed again.
            gameStrategies.remove(gameStrategy.getType());
            messageBus.announceWin(playerIdentity, gameStrategy);
            if (gameStrategies.isEmpty()) {
                // If all strategies have been won, the game is over.
                messageBus.announceGameOver();
            }
        } else {
            final String reason = String.format("Rejected winning claim from %s for game strategy %s since game card does not meet winning requirements.", playerIdentity, gameStrategy.getType());
            rejectClaim(playerIdentity, gameStrategy, gameCard, reason);
        }
    }

    protected void rejectClaim(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard, final String reason) {
        // We ignore the rejection, lets turn the other cheek. The show must go on!
    }

    @Override
    public void winningClaimEvent(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard) {
        if (playerIdentity == null || gameStrategy == null || gameCard == null) return;

        this.ajudicateClaim(playerIdentity, gameStrategy, gameCard);
    }

    @Override
    public void playerRegisteredEvent(final PlayerIdentity playerIdentity) {
        if (playerIdentity == null) return;

        players.add(playerIdentity);
    }
}
