package gk.bingo.game;


import gk.bingo.game.adjudicator.BingoAdjudicator;
import gk.bingo.game.announcer.BingoAnnouncer;
import gk.bingo.game.caller.BingoCaller;
import gk.bingo.game.player.Player;
import gk.bingo.game.registrar.BingoRegistrar;

/**
 * Standard implementation of a {@link BingoGame}.
 *
 * @since 1.0
 * @uthor Gordon Kelly
 */
class StandardBingoGame implements BingoGame {

    private final BingoAnnouncer bingoAnnouncer;
    private final BingoAdjudicator bingoAdjudicator;
    private final BingoCaller bingoCaller;
    private final BingoRegistrar bingoRegistrar;

    StandardBingoGame(final BingoRegistrar bingoRegistrar, final BingoCaller bingoCaller, final BingoAnnouncer bingoAnnouncer, final BingoAdjudicator bingoAdjudicator ) {
        this.bingoRegistrar = bingoRegistrar;
        this.bingoCaller = bingoCaller;
        this.bingoAnnouncer = bingoAnnouncer;
        this.bingoAdjudicator = bingoAdjudicator;
    }

    public final void register(final Player player) {
        bingoRegistrar.registerPlayer(player);
    }

    public void callNextNumber() {
        bingoCaller.callNextNumber();
    }

    @Override
    public String toString() {
        return "BingoGame{" +
                "bingoAnnouncer=" + bingoAnnouncer +
                ", bingoAdjudicator=" + bingoAdjudicator +
                ", bingoCaller=" + bingoCaller +
                ", bingoRegistrar=" + bingoRegistrar +
                '}';
    }
}
