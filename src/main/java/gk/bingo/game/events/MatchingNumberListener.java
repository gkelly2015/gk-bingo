package gk.bingo.game.events;

import gk.bingo.game.machine.BingoNumber;
import gk.bingo.game.registrar.PlayerIdentity;

/**
 * Allows a player to announce a matching number on the bingo game card.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface MatchingNumberListener {

    /**
     * Announce a matching number to the interested parties.
     * @param playerIdentity - PlayerIdentity
     * @param bingoNumber  - BingoNumber
     */
    void matchingNumberEvent(PlayerIdentity playerIdentity, BingoNumber bingoNumber);
}
