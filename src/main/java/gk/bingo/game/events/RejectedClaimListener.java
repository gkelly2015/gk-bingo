package gk.bingo.game.events;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

/**
 * Notify the world when player identified by {@link PlayerIdentity} has made a claim for {@link GameStrategy}
 * against {@link BingoGameCard} with the rejection reason.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface RejectedClaimListener {

    /**
     * Notify the world when a claim has been rejected.
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy  - GameStrategy
     * @param gameCard  - BingoGameCard
     * @param reason - String
     */
    void rejectedClaimEvent(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard, String reason);
}
