package gk.bingo.game.events;

/**
 * Allows a caller to announce the next number drawn from the bingo machine.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface NumberDrawnListener {

    /**
     * Notify the next number drawn from the bingo machine.
     * @param nextNumber - Integer
     */
    void numberDrawnEvent(Integer nextNumber);
}
