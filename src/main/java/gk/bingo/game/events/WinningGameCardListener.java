package gk.bingo.game.events;


import gk.bingo.game.BingoGameCard;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

/**
 * Notify the world when player identified by {@link PlayerIdentity} has won {@link GameStrategy}
 * based on the {@link BingoGameCard} provided.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface WinningGameCardListener {
    void winningGameCardEvent(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy);
}
