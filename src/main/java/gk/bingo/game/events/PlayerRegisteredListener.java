package gk.bingo.game.events;


import gk.bingo.game.registrar.PlayerIdentity;

/**
 * Notify the world when a new player has been registered in a game.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface PlayerRegisteredListener {

    /**
     * Notify the world player identified by {@link PlayerIdentity} has been registered in the game.
     * @param playerIdentity - PlayerIdentity
     */
    void playerRegisteredEvent(PlayerIdentity playerIdentity);
}
