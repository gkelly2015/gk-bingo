package gk.bingo.game.events;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.machine.BingoNumber;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Simple, potentially naive implementation of a message bus. It does not provide locking or thread
 * safety which would not fly in the real world.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
final class SimpleMessageBus implements MessageBus {

    private final Set<PlayerRegisteredListener> playerRegisteredListeners;
    private final Set<NumberDrawnListener> numberDrawnListeners;
    private final Set<MatchingNumberListener> matchingNumberListeners;
    private final Set<WinningClaimListener> winningClaimListeners;
    private final Set<WinningGameCardListener> winningGameCardListeners;
    private final Set<RejectedClaimListener> rejectedClaimListeners;
    private final Set<GameOverListener> gameOverListeners;

    SimpleMessageBus() {
        playerRegisteredListeners = new LinkedHashSet<>();
        numberDrawnListeners = new LinkedHashSet<>();
        matchingNumberListeners = new LinkedHashSet<>();
        winningClaimListeners = new LinkedHashSet<>();
        winningGameCardListeners = new LinkedHashSet<>();
        rejectedClaimListeners = new LinkedHashSet<>();
        gameOverListeners = new LinkedHashSet<>();
    }

    @Override
    public void registerPlayerRegistrationListener(final PlayerRegisteredListener playerRegisteredListener) {
        if (playerRegisteredListener != null) {
            playerRegisteredListeners.add(playerRegisteredListener);
        }
    }

    @Override
    public void registerNumberDrawnListener(final NumberDrawnListener numberDrawnListener) {
        if (numberDrawnListener != null) {
            numberDrawnListeners.add(numberDrawnListener);
        }
    }

    @Override
    public void registerMatchingNumberListener(final MatchingNumberListener matchingNumberListener) {
        if (matchingNumberListener != null) {
            matchingNumberListeners.add(matchingNumberListener);
        }
    }

    @Override
    public void registerWinningClaimListener(final WinningClaimListener winningClaimListener) {
        if (winningClaimListener != null) {
            winningClaimListeners.add(winningClaimListener);
        }
    }

    @Override
    public void registerWinningGameCardListener(final WinningGameCardListener winningGameCardListener) {
        if (winningGameCardListener != null) {
            winningGameCardListeners.add(winningGameCardListener);
        }
    }

    @Override
    public void registerRejectedClaimListener(final RejectedClaimListener rejectedClaimListener) {
        if (rejectedClaimListener != null) {
            rejectedClaimListeners.add(rejectedClaimListener);
        }
    }

    @Override
    public void registerGameOverListener(final GameOverListener gameOverListener) {
        if (gameOverListener != null) {
            gameOverListeners.add(gameOverListener);
        }
    }

    @Override
    public void deregisterPlayerRegistrationListener(PlayerRegisteredListener playerRegisteredListener) {
        if (playerRegisteredListener != null) {
            playerRegisteredListeners.remove(playerRegisteredListener);
        }
    }

    @Override
    public void deregisterNumberDrawnListener(final NumberDrawnListener numberDrawnListener) {
        if (numberDrawnListener != null) {
            numberDrawnListeners.remove(numberDrawnListener);
        }
    }

    @Override
    public void deregisterMatchingNumberListener(final MatchingNumberListener matchingNumberListener) {
        if (matchingNumberListener != null) {
            matchingNumberListeners.remove(matchingNumberListener);
        }
    }

    @Override
    public void deregisterWinningClaimListener(final WinningClaimListener winningClaimListener) {
        if (winningClaimListener != null) {
            winningClaimListeners.remove(winningClaimListener);
        }
    }

    @Override
    public void deregisterWinningGameCardListener(final WinningGameCardListener winningGameCardListener) {
        if (winningGameCardListener != null) {
            winningGameCardListeners.remove(winningGameCardListener);
        }
    }

    @Override
    public void deregisterRejectedClaimListener(final RejectedClaimListener rejectedClaimListener) {
        if (rejectedClaimListener != null) {
            rejectedClaimListeners.remove(rejectedClaimListener);
        }
    }

    @Override
    public void deregisterGameOverListener(final GameOverListener gameOverListener) {
        if (gameOverListener != null) {
            gameOverListeners.remove(gameOverListener);
        }
    }

    @Override
    public void announcePlayerRegistered(final PlayerIdentity playerIdentity) {
        playerRegisteredListeners.forEach(l -> l.playerRegisteredEvent(playerIdentity));
    }

    @Override
    public void announceNumberDrawn(final Integer nextNumber) {
        numberDrawnListeners.forEach(l -> l.numberDrawnEvent(nextNumber));
    }

    @Override
    public void announceMatchingNumber(final PlayerIdentity playerIdentity, final BingoNumber bingoNumber) {
        matchingNumberListeners.forEach(l -> l.matchingNumberEvent(playerIdentity, bingoNumber));
    }

    @Override
    public void claimWin(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard) {
        winningClaimListeners.forEach(l -> l.winningClaimEvent(playerIdentity, gameStrategy, gameCard));
    }

    @Override
    public void announceWin(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy) {
        winningGameCardListeners.forEach(l -> l.winningGameCardEvent(playerIdentity, gameStrategy));
    }

    @Override
    public void rejectClaim(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy, final BingoGameCard gameCard, final String reason) {
        rejectedClaimListeners.forEach(l -> l.rejectedClaimEvent(playerIdentity, gameStrategy, gameCard, reason));
    }

    @Override
    public void announceGameOver() {
        gameOverListeners.forEach(l -> l.gameOverEvent());
    }

    public static class Builder {

        public SimpleMessageBus build() {
            return new SimpleMessageBus();
        }

    }
}
