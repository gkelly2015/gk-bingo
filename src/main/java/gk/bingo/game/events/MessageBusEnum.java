package gk.bingo.game.events;

/**
 * Represents the catalog of MessageBusEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum MessageBusEnum {
    SIMPLE_MESSAGE_BUS;
}
