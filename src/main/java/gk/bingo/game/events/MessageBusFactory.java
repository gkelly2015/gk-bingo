package gk.bingo.game.events;

import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link MessageBusFactory}. All available concrete implementations are catalogued in the {@link MessageBusEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class MessageBusFactory implements AbstractFactory<MessageBusEnum, MessageBus> {


    @Override
    public MessageBus create(MessageBusEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("MessageBusEnum cannot be null");

        switch (discriminator) {
            case SIMPLE_MESSAGE_BUS:
                return new SimpleMessageBus();
            default:
                throw new IllegalArgumentException("Unsupported MessageBusFactoryEnum received: " + discriminator);
        }
    }
}
