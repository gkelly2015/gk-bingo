package gk.bingo.game.events;

/**
 * Allows the end of the game to be signalled to all listeners.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface GameOverListener {

    /**
     * Its game over.
     */
    void gameOverEvent();
}
