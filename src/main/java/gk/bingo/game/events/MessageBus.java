package gk.bingo.game.events;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.machine.BingoNumber;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

/**
 * Abstraction that allows a message bus to be implemented.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface MessageBus {

    /**
     * Register a {@link PlayerRegisteredListener}
     * @param playerRegisteredListener - PlayerRegisteredListener
     */
    void registerPlayerRegistrationListener(PlayerRegisteredListener playerRegisteredListener);

    /**
     * Register a {@link NumberDrawnListener}
     * @param numberDrawnListener - NumberDrawnListener
     */
    void registerNumberDrawnListener(NumberDrawnListener numberDrawnListener);

    /**
     * Register a {@link MatchingNumberListener}
     * @param matchingNumberListener - MatchingNumberListener
     */
    void registerMatchingNumberListener(MatchingNumberListener matchingNumberListener);

    /**
     * Register a {@link WinningClaimListener}
     * @param winningClaimListener - WinningClaimListener
     */
    void registerWinningClaimListener(WinningClaimListener winningClaimListener);

    /**
     * Register a {@link WinningGameCardListener}
     * @param winningGameCardListener - WinningGameCardListener
     */
    void registerWinningGameCardListener(WinningGameCardListener winningGameCardListener);

    /**
     * Register a {@link RejectedClaimListener}
     * @param rejectedClaimListener - RejectedClaimListener
     */
    void registerRejectedClaimListener(RejectedClaimListener rejectedClaimListener);

    /**
     * Register a {@link GameOverListener}
     * @param gameOverListener - GameOverListener
     */
    void registerGameOverListener(GameOverListener gameOverListener);

    /**
     * Dergister a {@link PlayerRegisteredListener}
     * @param playerRegisteredListener - PlayerRegisteredListener
     */
    void deregisterPlayerRegistrationListener(PlayerRegisteredListener playerRegisteredListener);

    /**
     * Deegister a {@link NumberDrawnListener}
     * @param numberDrawnListener - NumberDrawnListener
     */
    void deregisterNumberDrawnListener(NumberDrawnListener numberDrawnListener);

    /**
     * deegister a {@link MatchingNumberListener}
     * @param matchingNumberListener - MatchingNumberListener
     */
    void deregisterMatchingNumberListener(MatchingNumberListener matchingNumberListener);

    /**
     * Deegister a {@link WinningClaimListener}
     * @param winningClaimListener - WinningClaimListener
     */
    void deregisterWinningClaimListener(WinningClaimListener winningClaimListener);

    /**
     * Deegister a {@link WinningGameCardListener}
     * @param winningGameCardListener - WinningGameCardListener
     */
    void deregisterWinningGameCardListener(WinningGameCardListener winningGameCardListener);

    /**
     * Deegister a {@link RejectedClaimListener}
     * @param rejectedClaimListener - RejectedClaimListener
     */
    void deregisterRejectedClaimListener(RejectedClaimListener rejectedClaimListener);

    /**
     * Deegister a {@link GameOverListener}
     * @param gameOverListener - GameOverListener
     */
    void deregisterGameOverListener(GameOverListener gameOverListener);

    /**
     * Announce a {@link PlayerIdentity} has been registered in the game.
     * @param playerIdentity - PlayerIdentity
     */
    void announcePlayerRegistered(PlayerIdentity playerIdentity);

    /**
     * Announce when a new number has been drawn.
     * @param nextNumber - Integer
     */
    void announceNumberDrawn(Integer nextNumber);

    /**
     * Announce whena player identified by {@link PlayerIdentity} has matched {@link BingoNumber}.
     * @param playerIdentity - PlayerIdentity
     * @param bingoNumber - BingoNumber
     */
    void announceMatchingNumber(PlayerIdentity playerIdentity, BingoNumber bingoNumber);

    /**
     * Claim a win. Player identified by {@link PlayerIdentity} is claiming a win based on their
     * {@link BingoGameCard} and {@link GameStrategy}.
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy - GameStrategy
     * @param gameCard - BingoGameCard
     */
    void claimWin(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard);

    /**
     * Announce a winner! When an adjudicator has confirmed a win by the player identified
     * by {@link PlayerIdentity} has a winning {@link GameStrategy}.
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy - GameStrategy
     */
    void announceWin(PlayerIdentity playerIdentity, GameStrategy gameStrategy);

    /**
     * Claim rejected! When an adjudicator has confirmed a claim made by the player identified
     * by {@link PlayerIdentity} for a {@link GameStrategy} and {@link BingoGameCard} that does
     * not satisfy the constraints of the {@link GameStrategy}.
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy - GameStrategy
     * @param gameCard - BingoGameCard
     * @param reason - String
     */
    void rejectClaim(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard, String reason);

    /**
     * Announce game over.
     */
    void announceGameOver();


}
