package gk.bingo.game.events;


import gk.bingo.game.BingoGameCard;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

/**
 * Notify the adjuhdicator when the player identified by {@link PlayerIdentity} is claiming to have won {@link GameStrategy}
 * based on the {@link BingoGameCard} provided.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface WinningClaimListener {

    /**
     * Notify the world, we have a winner!
     * @param playerIdentity - PlayerIdentity
     * @param gameStrategy - GameStrategy
     * @param gameCard - BingoGameCard
     */
    void winningClaimEvent(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard);
}
