package gk.bingo.game.registrar;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.BingoGameCardEnum;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.player.Player;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;
import gk.patterns.AbstractFactory;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Standard implementation of a {@link BingoRegistrar}. When assigning the set of game strategies, its important to
 * note they are stateful implementations, and as such, a new representation in memory must be created for each
 * player. Care must be taken to ensure thread-safety in a multi-threaded environment.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class StandardRegistrar implements BingoRegistrar {

    private final MessageBus messageBus;
    private final AtomicInteger playerSerialNumbers;
    private final AbstractFactory<GameStrategyEnum, GameStrategy> gameStrategyFactory;
    private final AbstractFactory<BingoGameCardEnum, BingoGameCard> gameCardFactory;
    private final BingoGameCardEnum gameCardEnum;

    StandardRegistrar(final MessageBus messageBus, final AbstractFactory<GameStrategyEnum, GameStrategy> gameStrategyFactory,
                      final AbstractFactory<BingoGameCardEnum, BingoGameCard> gameCardFactory, final BingoGameCardEnum gameCardEnum) {
        this.messageBus = messageBus;
        this.gameStrategyFactory = gameStrategyFactory;
        this.gameCardFactory = gameCardFactory;
        this.gameCardEnum = gameCardEnum;
        playerSerialNumbers = new AtomicInteger();
    }


    @Override
    public void registerPlayer(Player player) {
        final String id = String.format("Player#%d", playerSerialNumbers.incrementAndGet());
        final BingoGameCard gameCard = gameCardFactory.create(gameCardEnum);
        final PlayerIdentity playerIdentity = new PlayerIdentity(id);

        final GamePacket gamePacket = new GamePacket(playerIdentity, gameCard, buildGameStrategies());
        player.receiveGamePacket(gamePacket);

        messageBus.announcePlayerRegistered(playerIdentity);
    }

    protected Map<GameStrategyEnum, GameStrategy> buildGameStrategies() {
        final Map<GameStrategyEnum, GameStrategy> strategies = new EnumMap<>(GameStrategyEnum.class);
        Stream.of(GameStrategyEnum.values()).forEach(gameStrategyEnum -> {
            strategies.put(gameStrategyEnum, gameStrategyFactory.create(gameStrategyEnum));
        });
        return Collections.synchronizedMap(strategies);
    }
}
