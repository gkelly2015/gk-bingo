package gk.bingo.game.registrar;

/**
 * Represents the catalog of BingoRegistrarEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoRegistrarEnum {
    STANDARD_REGISTRAR;
}
