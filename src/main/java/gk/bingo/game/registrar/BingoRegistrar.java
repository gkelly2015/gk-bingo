package gk.bingo.game.registrar;

import gk.bingo.game.player.Player;

/**
 * Bingo registrar is responsible for registering a player in the game. Generally a registrar will
 * provide the {@link Player} with a unique identity, a bingo game card, and a set of game algorithms
 * that are in play for this game.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoRegistrar {


    /**
     * Register the {@link Player} in the game.
     * @param player - Player
     */
    void registerPlayer(final Player player);
}
