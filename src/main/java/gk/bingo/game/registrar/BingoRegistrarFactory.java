package gk.bingo.game.registrar;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.BingoGameCardEnum;
import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoRegistrar}. All available concrete implementations are catalogued in the {@link BingoRegistrarEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoRegistrarFactory implements AbstractFactory<BingoRegistrarEnum, BingoRegistrar> {

    private final MessageBus messageBus;
    private final BingoGameParameters bingoGameParameters;
    private final AbstractFactory<GameStrategyEnum, GameStrategy> gameStrategyFactory;
    private final AbstractFactory<BingoGameCardEnum, BingoGameCard> gameCardFactory;
    private final BingoGameCardEnum gameCardType;

    public BingoRegistrarFactory(final MessageBus messageBus, final BingoGameParameters bingoGameParameters, final AbstractFactory<GameStrategyEnum, GameStrategy> gameStrategyFactory,
                                 final AbstractFactory<BingoGameCardEnum, BingoGameCard> gameCardFactory, final BingoGameCardEnum gameCardType) {
        this.messageBus = messageBus;
        this.bingoGameParameters = bingoGameParameters;
        this.gameStrategyFactory = gameStrategyFactory;
        this.gameCardFactory = gameCardFactory;
        this.gameCardType = gameCardType;
    }

    @Override
    public BingoRegistrar create(final BingoRegistrarEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("BingoRegistrarEnum must not be null");

        switch (discriminator) {
            case STANDARD_REGISTRAR:
                return new StandardRegistrar(messageBus, gameStrategyFactory, gameCardFactory, gameCardType);
            default:
                throw new IllegalArgumentException("Unsupported BingoRegistrarEnum requested: " + discriminator);
        }
    }
}
