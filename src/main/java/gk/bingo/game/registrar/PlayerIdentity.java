package gk.bingo.game.registrar;

/**
 * Represents a unique player in the bingo game. Instances will be created by
 * the registrar and no mutations can be made.
 *
 * Note this class is final since it represents the identify of the player and
 * we do not wish to allow the identify to be manipulated in any way.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public final class PlayerIdentity {

    private String id;

    PlayerIdentity(final String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerIdentity)) return false;

        PlayerIdentity that = (PlayerIdentity) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
