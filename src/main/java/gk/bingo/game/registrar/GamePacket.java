package gk.bingo.game.registrar;


import gk.bingo.game.BingoGameCard;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;

import java.util.Map;

/**
 * Game packet is a wrapper class to provide access to the {@link PlayerIdentity} {@link BingoGameCard} and the set
 * of {@link GameStrategy} currently in play for the game.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class GamePacket {

    private final PlayerIdentity playerIdentity;
    private final BingoGameCard gameCard;
    private final Map<GameStrategyEnum, GameStrategy> gameStrategies;

    GamePacket(final PlayerIdentity playerIdentity, final BingoGameCard gameCard, final Map<GameStrategyEnum, GameStrategy> gameStrategies) {
        this.playerIdentity = playerIdentity;
        this.gameCard = gameCard;
        this.gameStrategies = gameStrategies;
    }

    public PlayerIdentity getPlayerIdentity() {
        return playerIdentity;
    }

    public BingoGameCard getGameCard() {
        return gameCard;
    }

    public Map<GameStrategyEnum, GameStrategy> getGameStrategies() {
        return gameStrategies;
    }
}
