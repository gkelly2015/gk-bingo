package gk.bingo.game.io;

import gk.bingo.game.BingoGameParameters;

public class NumbersPerRowParser implements InputParser{

    private final BingoGameParameters.Builder builder;

    public NumbersPerRowParser(final BingoGameParameters.Builder builder) {
        this.builder = builder;
    }

    @Override
    public void prompt() {
        System.out.print("Enter numbers per row. Default to 5: ");
    }

    @Override
    public boolean accept(final String input) {
        boolean accept = false;
        try {
            if (input.isEmpty()) {
                return true;
            }
            final Integer value = Integer.parseInt(input);
            if (value > 0) {
                builder.withNumbersPerRow(value);
                accept = true;
            } else {
                System.out.println("Please specify a positive integer, greater than 0");
            }
        } catch (Exception e) {
            System.out.println("Please specify a positive integer, greater than 0");
        }
        return accept;
    }
};
