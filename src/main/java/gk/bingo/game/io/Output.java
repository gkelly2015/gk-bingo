package gk.bingo.game.io;

public interface Output {

    void write(String message);
}
