package gk.bingo.game.io;


import gk.bingo.game.BingoGameParameters;

public class NumberOfPlayersParser implements InputParser {

    private final BingoGameParameters.Builder builder;

    public NumberOfPlayersParser(final BingoGameParameters.Builder builder) {
        this.builder = builder;
    }

    @Override
    public void prompt() {
        System.out.print("Enter number of players playing the game: ");
    }

    @Override
    public boolean accept(final String input) {
        boolean accept = false;
        try {
            final Integer value = Integer.parseInt(input);
            if (value > 1) {
                builder.withNumberOfPlayers(value);
                accept = true;
            } else {
                System.out.println("Please specify a positive integer, greater than 1");
            }
        } catch (Exception e) {
            System.out.println("Please specify a positive integer, greater than 1");
        }
        return accept;
    }
};
