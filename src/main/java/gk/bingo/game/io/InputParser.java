package gk.bingo.game.io;

public interface InputParser {

    void prompt();

    boolean accept(String input);

}
