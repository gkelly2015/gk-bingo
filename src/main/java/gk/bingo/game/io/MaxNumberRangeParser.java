package gk.bingo.game.io;


import gk.bingo.game.BingoGameParameters;

public class MaxNumberRangeParser implements InputParser {

    private final BingoGameParameters.Builder builder;

    public MaxNumberRangeParser(final BingoGameParameters.Builder builder) {
        this.builder = builder;
    }

    @Override
    public void prompt() {
        System.out.print("Enter the number range [1-n]. Default is 90: ");
    }

    @Override
    public boolean accept(final String input) {
        boolean accept = false;
        try {
            if (input.isEmpty()) {
                return true;
            }
            final Integer value = Integer.parseInt(input);
            if (value > 0) {
                builder.withMaxNumberRange(value);
                accept = true;
            } else {
                System.out.println("Please specify a positive integer.");
            }
        } catch (Exception e) {
            System.out.println("Please specify a positive integer.");
        }
        return accept;
    }
};
