package gk.bingo.game.io;


import gk.bingo.game.BingoGameParameters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TicketSizeParser implements InputParser {

    private final BingoGameParameters.Builder builder;
    private static Pattern regex = Pattern.compile("^(\\d+)X(\\d+)$");

    public TicketSizeParser(final BingoGameParameters.Builder builder) {
        this.builder = builder;
    }

    @Override
    public void prompt() {
        System.out.print("Enter ticket size. Default 3X10 (3 rows and 10 columns): ");
    }

    @Override
    public boolean accept(final String input) {
        if (input.isEmpty()) {
            return true;
        }
        final Matcher m = regex.matcher(input);
        if (m.matches()) {
            final Integer rows = Integer.parseInt(m.group(1));
            final Integer columns = Integer.parseInt(m.group(2));
            builder.withNumberOfRows(rows) ;
            builder.withNumberOfColumns(columns);
            return true;
        } else {
            System.out.println("Invalid entry, please enter in the form rowsXcols e.g. 3X10");
        }
        return false;
    }
}
