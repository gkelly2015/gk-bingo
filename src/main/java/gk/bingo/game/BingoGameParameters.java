package gk.bingo.game;

import gk.bingo.game.algorithms.ShuffleAlgorithm;
import gk.bingo.game.algorithms.ShuffleAlgorithmEnum;
import gk.bingo.game.algorithms.ShuffleAlgorithmFactory;
import gk.patterns.AbstractFactory;

/**
 * Represents the parameters provided at starup that defines how the bingo game should be setup.
 * The Builder design pattern has been employed during the design of this class.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoGameParameters {

    private final ShuffleAlgorithm shuffleAlgorithm;
    private final Integer minNumberRange;
    private final Integer maxNumberRange;
    private final Integer numberOfPlayers;
    private final Integer numberOfColumns;
    private final Integer numberOfRows;
    private final Integer numbersPerRow;

    private BingoGameParameters(final Builder builder) {
        this.shuffleAlgorithm = builder.shuffleAlgorithm;
        this.minNumberRange = builder.minNumberRange;
        this.maxNumberRange = builder.maxNumberRange;
        this.numberOfPlayers = builder.numberOfPlayers;
        this.numberOfColumns = builder.numberOfColumns;
        this.numberOfRows = builder.numberOfRows;
        this.numbersPerRow = builder.numbersPerRow;
    }

    public ShuffleAlgorithm getShuffleAlgorithm() {
        return shuffleAlgorithm;
    }

    public Integer getMinNumberRange() {
        return minNumberRange;
    }

    public Integer getMaxNumberRange() {
        return maxNumberRange;
    }

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public Integer getNumberOfColumns() {
        return numberOfColumns;
    }

    public Integer getNumberOfRows() {
        return numberOfRows;
    }

    public Integer getNumbersPerRow() {
        return numbersPerRow;
    }

    public Integer getNumbersPerCard() {
        return numberOfRows * numbersPerRow;
    }

    public void validate() {
        if (numberOfPlayers == null) {
            throw new IllegalArgumentException("Number of players must be a positive integer.");
        }
        if (maxNumberRange == null) {
            throw new IllegalArgumentException("Max number range must be a positive integer.");
        }
        if (numberOfColumns == null) {
            throw new IllegalArgumentException("Number of columns must be a positive integer.");
        }
        if (numberOfRows == null) {
            throw new IllegalArgumentException("Number of rows must be a positive integer.");
        }
        if (numbersPerRow == null) {
            throw new IllegalArgumentException("Numbers per row must be a positive integer.");
        }
        if (shuffleAlgorithm == null) {
            throw new IllegalArgumentException("Shuffle algorithm cannot be null.");
        }
        if (numberOfRows * numbersPerRow > maxNumberRange) {
            throw new IllegalArgumentException(String.format("Insufficient number range specified " +
                    "(%d) for a ticket with %d rows and %d numbers per row.", maxNumberRange, numberOfRows, numbersPerRow));
        }
    }

    public static class Builder {
        private final AbstractFactory<ShuffleAlgorithmEnum, ShuffleAlgorithm> shuffleAlgorithmFactory;
        private ShuffleAlgorithm shuffleAlgorithm;
        private Integer minNumberRange = 1;
        private Integer maxNumberRange = 90;
        private Integer numberOfPlayers = 2;
        private Integer numberOfColumns = 10;
        private Integer numberOfRows = 3;
        private Integer numbersPerRow = 5;

        public Builder() {
            this.shuffleAlgorithmFactory = new ShuffleAlgorithmFactory();
            this.shuffleAlgorithm = shuffleAlgorithmFactory.create(ShuffleAlgorithmEnum.FISHER_YATES);
        }

        public Builder withShuffleAlgorithmEnum(final ShuffleAlgorithmEnum shuffleAlgorithmEnum) {
            this.shuffleAlgorithm = shuffleAlgorithmFactory.create(shuffleAlgorithmEnum);
            return this;
        }

        public Builder withMinNumberRange(final Integer minNumberRange) {
            this.minNumberRange = minNumberRange;
            return this;
        }

        public Builder withMaxNumberRange(final Integer maxNumberRange) {
            this.maxNumberRange = maxNumberRange;
            return this;
        }

        public Builder withNumberOfPlayers(final Integer numberOfPlayers) {
            this.numberOfPlayers = numberOfPlayers;
            return this;
        }

        public Builder withNumberOfColumns(final Integer numberOfColumns) {
            this.numberOfColumns = numberOfColumns;
            return this;
        }

        public Builder withNumberOfRows(final Integer numberOfRows) {
            this.numberOfRows = numberOfRows;
            return this;
        }

        public Builder withNumbersPerRow(final Integer numbersPerRow) {
            this.numbersPerRow = numbersPerRow;
            return this;
        }

        public BingoGameParameters build() {
            return new BingoGameParameters(this);
        }
    }

}
