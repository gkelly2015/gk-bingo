package gk.bingo.game.algorithms;

/**
 * Represents the catalog of ShuffleAlgorithmEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum ShuffleAlgorithmEnum {
    FISHER_YATES;
}
