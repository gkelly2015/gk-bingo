package gk.bingo.game.algorithms;

/**
 * Interface to allow varied implementations of shuffle to be provided to the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface ShuffleAlgorithm {

    /**
     * Accept an array and perform an in-place, or destructive shuffle.
     * @param array - int[]
     */
    void shuffle(int[] array);
}
