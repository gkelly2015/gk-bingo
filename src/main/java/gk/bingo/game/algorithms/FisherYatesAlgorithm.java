package gk.bingo.game.algorithms;

import java.util.Random;

/**
 * Implementation of the Fisher Yates Algorithm for sorting.
 * @see <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">Fisher Yates Shuffle Algorithm</a>.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
final class FisherYatesAlgorithm implements ShuffleAlgorithm {

    @Override
    public void shuffle(final int[] array) {
        final Random random = new Random();
        for (int index = array.length - 1; index > 0; index--) {
            final int rand = random.nextInt(index+1);

            final int swap = array[index];
            array[index] = array[rand];
            array[rand] = swap;
        }
    }
}
