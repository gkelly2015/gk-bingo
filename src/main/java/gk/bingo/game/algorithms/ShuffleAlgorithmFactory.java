package gk.bingo.game.algorithms;

import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link ShuffleAlgorithm}. All available concrete implementations are catalogued in the {@link ShuffleAlgorithmEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class ShuffleAlgorithmFactory implements AbstractFactory<ShuffleAlgorithmEnum, ShuffleAlgorithm> {

    @Override
    public ShuffleAlgorithm create(ShuffleAlgorithmEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("ShuffleAlgorithmEnum cannot be null");

        switch (discriminator) {
            case FISHER_YATES:
                return new FisherYatesAlgorithm();
            default:
                throw new IllegalArgumentException("Unsupported ShuffleAlgorithmEnum received: " + discriminator);
        }
    }
}
