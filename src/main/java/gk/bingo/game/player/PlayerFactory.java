package gk.bingo.game.player;

import gk.bingo.game.events.MessageBus;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link Player}. All available concrete implementations are catalogued in the {@link PlayerEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class PlayerFactory implements AbstractFactory<PlayerEnum, Player> {

    private final MessageBus messageBus;

    public PlayerFactory(final MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    @Override
    public Player create(final PlayerEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("PlayerEnum cannot be null");

        switch (discriminator) {
            case STANDARD_PLAYER:
                return new StandardPlayer(messageBus);
            case BAD_ACTOR:
                return new BadActor(messageBus);
            default:
                throw new IllegalArgumentException("Unsupported PlayerEnum received: " + discriminator);

        }
    }
}
