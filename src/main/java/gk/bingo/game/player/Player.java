package gk.bingo.game.player;

import gk.bingo.game.events.NumberDrawnListener;
import gk.bingo.game.events.WinningGameCardListener;
import gk.bingo.game.registrar.GamePacket;
import gk.bingo.game.registrar.PlayerIdentity;

/**
 * Represents a player in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface Player extends NumberDrawnListener, WinningGameCardListener {

    /**
     * Provide the identity of the player.
     * @return - PlayerIdentity
     */
    PlayerIdentity getPlayerIdentity();

    /**
     * When a player registers to participate in a game of bingo, a {@link GamePacket}
     * will be provided that sets the player's identity, provides the bingo game card  and
     * the set of game strategies currently in play.
     * @param gamePacket - GamePacket
     */
    void receiveGamePacket(final GamePacket gamePacket);

}
