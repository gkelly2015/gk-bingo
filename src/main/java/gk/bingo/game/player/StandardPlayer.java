package gk.bingo.game.player;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.machine.BingoNumber;
import gk.bingo.game.registrar.GamePacket;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;

import java.util.EnumSet;
import java.util.Map;

/**
 * Standard player, follows all of the rules and never makes fraudlent claims.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class StandardPlayer implements Player {

    private PlayerIdentity playerIdentity;
    private BingoGameCard gameCard;
    private Map<GameStrategyEnum, GameStrategy> gameStrategies;
    private EnumSet<GameStrategyEnum> activeGameStrategies;
    private MessageBus messageBus;

    StandardPlayer(final MessageBus messageBus) {
        this.messageBus = messageBus;
        messageBus.registerNumberDrawnListener(this);
        messageBus.registerWinningGameCardListener(this);
    }

    public PlayerIdentity getPlayerIdentity() {
        return playerIdentity;
    }

    public void receiveGamePacket(final GamePacket gamePacket) {
        this.playerIdentity = gamePacket.getPlayerIdentity();
        this.gameCard = gamePacket.getGameCard();
        this.gameStrategies = gamePacket.getGameStrategies();
        this.activeGameStrategies = EnumSet.copyOf(gameStrategies.keySet());
    }

    @Override
    public void numberDrawnEvent(final Integer number) {
        if (gameCard == null) return;

        final BingoNumber bingoNumber = gameCard.checkNumber(number);
        if (bingoNumber != null) {
            messageBus.announceMatchingNumber(playerIdentity, bingoNumber);

            gameStrategies.entrySet().forEach(e -> {
                GameStrategyEnum gameStrategyType = e.getKey();
                GameStrategy gameStrategy = e.getValue();
                if (activeGameStrategies.contains(gameStrategyType) && gameStrategy.isWinning(bingoNumber, gameCard)) {
                    messageBus.claimWin(playerIdentity, gameStrategy, gameCard);
                }
            });
        }
    }

    @Override
    public void winningGameCardEvent(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy) {
        gameStrategies.remove(gameStrategy.getType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StandardPlayer)) return false;

        StandardPlayer player = (StandardPlayer) o;

        if (playerIdentity != null ? !playerIdentity.equals(player.playerIdentity) : player.playerIdentity != null)
            return false;
        return gameCard != null ? gameCard.equals(player.gameCard) : player.gameCard == null;

    }

    @Override
    public int hashCode() {
        int result = playerIdentity != null ? playerIdentity.hashCode() : 0;
        result = 31 * result + (gameCard != null ? gameCard.hashCode() : 0);
        return result;
    }
}
