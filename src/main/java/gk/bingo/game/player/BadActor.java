package gk.bingo.game.player;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.registrar.GamePacket;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Bad guy that tries to defraud the bingo game by making false claims for wins when
 * none exist. Watch out for this guy!
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class BadActor implements Player {

    private static final Integer NUMBER_OF_DRAWS_TO_WAIT_BEFORE_MAKING_FRAUDLENT_CLAIM = 5;
    private final AtomicInteger numberOfNumbersDrawn;
    private final MessageBus messageBus;
    private PlayerIdentity playerIdentity;
    private BingoGameCard gameCard;
    private Map<GameStrategyEnum, GameStrategy> gameStrategies;

    BadActor(final MessageBus messageBus) {
        this.messageBus = messageBus;
        numberOfNumbersDrawn = new AtomicInteger();
        System.out.println("Watch out, watch out where ever you are! Mwuhahahahah!");
        messageBus.registerNumberDrawnListener(this);
    }

    @Override
    public void numberDrawnEvent(Integer nextNumber) {
        if (NUMBER_OF_DRAWS_TO_WAIT_BEFORE_MAKING_FRAUDLENT_CLAIM.compareTo(numberOfNumbersDrawn.getAndIncrement()) == 0) {
            System.out.println("Mwuhahaha making fraudlent claim! " + playerIdentity);
            final GameStrategy gameStrategy = gameStrategies.entrySet().iterator().next().getValue();
            messageBus.claimWin(playerIdentity, gameStrategy, gameCard);
        }
    }

    @Override
    public void winningGameCardEvent(PlayerIdentity playerIdentity, GameStrategy gameStrategy) {

    }

    @Override
    public PlayerIdentity getPlayerIdentity() {
        return null;
    }

    @Override
    public void receiveGamePacket(GamePacket gamePacket) {
        this.playerIdentity = gamePacket.getPlayerIdentity();
        this.gameCard = gamePacket.getGameCard();
        this.gameStrategies = gamePacket.getGameStrategies();

        System.out.println("Watch out for me, I'm a bad actor!! " + playerIdentity);
    }
}
