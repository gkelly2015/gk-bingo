package gk.bingo.game.player;

/**
 * Represents the catalog of PlayerEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum PlayerEnum {
    STANDARD_PLAYER,
    BAD_ACTOR;
}
