package gk.bingo.game.strategies;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.machine.BingoNumber;
import gk.datastructure.BinarySearchTree;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Top line strategy can be claimed by the first player to match all numbers on the top line of the bingo game card.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
final class TopLineGameStrategy implements GameStrategy {

    private final static Integer TOP_ROW_INDEX = 1;
    private final AtomicInteger topLineCount;
    private final Integer numbersPerRow;

    TopLineGameStrategy(final BingoGameParameters bingoGameParameters) {
        this.topLineCount = new AtomicInteger();
        numbersPerRow = bingoGameParameters.getNumbersPerRow();
    }

    @Override
    public boolean isWinning(final BingoNumber bingoNumber, final BingoGameCard gameCard) {
        return checkWinningCondition(topLineCount, bingoNumber);
    }

    @Override
    public boolean validate(final BingoGameCard gameCard, final BinarySearchTree<Integer, Integer> drawnNumbers) {
        final AtomicInteger confirmedTopFive = new AtomicInteger();
        final Iterator<Integer> i = drawnNumbers.keyIterator();
        while (i.hasNext()) {
            final Integer drawnNumber = i.next();
            final BingoNumber match = gameCard.checkNumber(drawnNumber);
            if (match != null && checkWinningCondition(confirmedTopFive, match)) {
                return true;
            }
        }
        return false;
    }

    boolean checkWinningCondition(final AtomicInteger count, final BingoNumber bingoNumber) {
        return TOP_ROW_INDEX.equals(bingoNumber.getRow()) && numbersPerRow.equals(count.incrementAndGet());
    }

    @Override
    public GameStrategyEnum getType() {
        return GameStrategyEnum.TOP_LINE;
    }
}
