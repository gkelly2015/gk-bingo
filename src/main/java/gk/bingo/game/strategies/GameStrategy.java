package gk.bingo.game.strategies;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.machine.BingoNumber;
import gk.datastructure.BinarySearchTree;

/**
 * Allows game strategies to be plugged into the system. Concrete implementations must provide
 * the algorithm to identify a winning strategy. It also must be able to answer to the adjudicator
 * when a player claims to have won the game strategy, in this case it will be given the set of
 * numbers already drawn and aseked to validate the player has correctly claimed the win.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface GameStrategy {

    boolean isWinning(BingoNumber bingoNumber, BingoGameCard gameCard);

    boolean validate(BingoGameCard gameCard, BinarySearchTree<Integer, Integer> drawnNumbers);

    GameStrategyEnum getType();

}
