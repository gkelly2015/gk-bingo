package gk.bingo.game.strategies;

/**
 * Represents the catalog of GameStrategyEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum GameStrategyEnum {

    EARLY_FIVE,
    FULL_HOUSE,
    TOP_LINE;

}
