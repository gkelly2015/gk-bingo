package gk.bingo.game.strategies;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.BingoGameParameters;
import gk.bingo.game.machine.BingoNumber;
import gk.datastructure.BinarySearchTree;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Full House implementation can be claimed by the first player to match all numbers on their
 * {@link BingoGameCard}
 *
 * @since 1.0
 * @author Gordon Kelly
 */
final class FullHouseGameStrategy implements GameStrategy {

    private final AtomicInteger fullHouseCount;
    private final Integer numberPerCard;

    FullHouseGameStrategy(final BingoGameParameters bingoGameParameters) {
        this.fullHouseCount = new AtomicInteger();
        this.numberPerCard = bingoGameParameters.getNumbersPerCard();
    }

    @Override
    public boolean isWinning(final BingoNumber bingoNumber, final BingoGameCard gameCard) {
        return checkWinningCondition(fullHouseCount);
    }

    @Override
    public boolean validate(final BingoGameCard gameCard, final BinarySearchTree<Integer, Integer> drawnNumbers) {
        final AtomicInteger confirmedMatchCount = new AtomicInteger();
        final Iterator<Integer> i = drawnNumbers.keyIterator();
        while (i.hasNext()) {
            final BingoNumber match = gameCard.checkNumber(i.next());
            if (match != null && checkWinningCondition(confirmedMatchCount) ) {
                return true;
            }
        }
        return false;
    }

    public boolean checkWinningCondition(final AtomicInteger count) {
        return numberPerCard.equals(count.incrementAndGet());
    }

    @Override
    public GameStrategyEnum getType() {
        return GameStrategyEnum.FULL_HOUSE;
    }
}
