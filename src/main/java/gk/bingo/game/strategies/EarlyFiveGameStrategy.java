package gk.bingo.game.strategies;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.machine.BingoNumber;
import gk.datastructure.BinarySearchTree;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Simple implementation of a {@link GameStrategy}.
 *
 * The rules are simple, the first player to match any five numbers on their card are eligible to claim
 * this {@link GameStrategy}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
final class EarlyFiveGameStrategy implements GameStrategy {

    private final static Integer FIVE = 5;
    private final AtomicInteger earlyFiveCount;

    EarlyFiveGameStrategy() {
        this.earlyFiveCount = new AtomicInteger();
    }

    @Override
    public boolean isWinning(final BingoNumber bingoNumber, final BingoGameCard gameCard) {
        return checkWinningCondition(earlyFiveCount);
    }

    @Override
    public boolean validate(final BingoGameCard gameCard, final BinarySearchTree<Integer, Integer> drawnNumbers) {
        final AtomicInteger confirmedMatchCount = new AtomicInteger();
        final Iterator<Integer> i = drawnNumbers.keyIterator();
        while (i.hasNext()) {
            final BingoNumber match = gameCard.checkNumber(i.next());
            if (match != null && checkWinningCondition(confirmedMatchCount)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkWinningCondition(final AtomicInteger count){
        return FIVE.equals(count.incrementAndGet());
    }

    @Override
    public GameStrategyEnum getType() {
        return GameStrategyEnum.EARLY_FIVE;
    }


}
