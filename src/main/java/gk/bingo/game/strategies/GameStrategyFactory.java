package gk.bingo.game.strategies;

import gk.bingo.game.BingoGameParameters;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link GameStrategy}. All available concrete implementations are catalogued in the {@link GameStrategyEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class GameStrategyFactory implements AbstractFactory<GameStrategyEnum, GameStrategy> {

    private final BingoGameParameters bingoGameParameters;

    public GameStrategyFactory(final BingoGameParameters bingoGameParameters) {
        this.bingoGameParameters = bingoGameParameters;
    }

    @Override
    public GameStrategy create(final GameStrategyEnum gameStrategyEnum) {
        if (gameStrategyEnum == null) return null;

        switch (gameStrategyEnum) {
            case EARLY_FIVE:
                return new EarlyFiveGameStrategy();
            case FULL_HOUSE:
                return new FullHouseGameStrategy(bingoGameParameters);
            case TOP_LINE:
                return new TopLineGameStrategy(bingoGameParameters);
            default:
                throw new IllegalArgumentException(String.format("Unsupported game strategy requested: %s", gameStrategyEnum));
        }
    }
}
