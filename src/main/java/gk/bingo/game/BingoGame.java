package gk.bingo.game;

import gk.bingo.game.player.Player;

/**
 * Bingo game responsibilities are to allow {@link Player}'s to register to participate in the game, and to
 * allow an external process to request the caller to drawn the next number from the bingo machine.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoGame {

    /**
     * Register a {@link Player} to participate in the game. A player in return will receive an identity,
     * a bingo game card, and a set of game strategies that can be used to identify a winning card.
     * @param player - Player
     */
    void register(Player player);

    /**
     * When an external actor requests the caller to drawn the next number from the bingo machine.
     */
    void callNextNumber() ;

}
