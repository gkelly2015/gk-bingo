package gk.bingo.game.caller;

/**
 * A BingCaller has one single responsibility. This is to draw the next number from the bingo machine.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoCaller {

    /**
     * Call the next number and broadcast to the event listeners.
     */
    void callNextNumber();
}
