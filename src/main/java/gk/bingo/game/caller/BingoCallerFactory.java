package gk.bingo.game.caller;

import gk.bingo.game.events.MessageBus;
import gk.bingo.game.machine.BingoMachine;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoCaller}. All available concrete implementations are catalogued in the {@link BingoCallerEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoCallerFactory implements AbstractFactory<BingoCallerEnum, BingoCaller> {

    private final MessageBus messageBus;
    private final BingoMachine bingoMachine;

    public BingoCallerFactory(final MessageBus messageBus,  final BingoMachine bingoMachine) {
        this.messageBus = messageBus;
        this.bingoMachine = bingoMachine;
    }

    @Override
    public BingoCaller create(BingoCallerEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("BingoCallerEnum cannot be null");

        switch(discriminator) {
            case STANDARD_CALLER:
                return new StandardBingoCaller(messageBus, bingoMachine);
            default:
                throw new IllegalArgumentException("Unsupported BingoAnnouncerEnum type received: " + discriminator);
        }
    }
}
