package gk.bingo.game.caller;

import gk.bingo.game.events.MessageBus;
import gk.bingo.game.machine.BingoMachine;

/**
 * Simple implementation of {@link BingoCaller} that announces the numbers as drawn from the {@link BingoMachine}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class StandardBingoCaller implements BingoCaller {

    private final MessageBus messageBus;
    private final BingoMachine bingoMachine;

    StandardBingoCaller(final MessageBus messageBus, final BingoMachine bingoMachine) {
        this.messageBus = messageBus;
        this.bingoMachine = bingoMachine;
    }

    @Override
    public void callNextNumber() {
        final Integer nextNumber = bingoMachine.nextNumber();
        if (nextNumber != null) {
            messageBus.announceNumberDrawn(nextNumber);
        } else {
            messageBus.announceGameOver();
        }
    }
}
