package gk.bingo.game.caller;

/**
 * Represents the catalog of BingoCallerEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoCallerEnum {
    STANDARD_CALLER;
}

