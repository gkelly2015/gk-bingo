package gk.bingo.game.announcer;

import gk.bingo.game.BingoGameCard;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.machine.BingoNumber;
import gk.bingo.game.registrar.PlayerIdentity;
import gk.bingo.game.strategies.GameStrategy;

import java.util.*;

/**
 * Implementation of {@link BingoAnnouncer} that communicates with the world via standard out.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class ConsoleBingoAnnouncer implements BingoAnnouncer {

    private final MessageBus messageBus;
    private final Map<PlayerIdentity, Set<GameStrategy>> winners = new HashMap<>();
    private final Set<PlayerIdentity> players = new LinkedHashSet<>();

    ConsoleBingoAnnouncer(final MessageBus messageBus) {
        this.messageBus = messageBus;
        this.messageBus.registerPlayerRegistrationListener(this);
        this.messageBus.registerGameOverListener(this);
        this.messageBus.registerWinningGameCardListener(this);
        this.messageBus.registerNumberDrawnListener(this);
        this.messageBus.registerMatchingNumberListener(this);
        this.messageBus.registerRejectedClaimListener(this);
    }

    @Override
    public void numberDrawnEvent(Integer number) {
        if (number == null) return;

        System.out.printf("Next number is: %02d\n", number);
    }

    @Override
    public void winningGameCardEvent(final PlayerIdentity playerIdentity, final GameStrategy gameStrategy) {
        if (playerIdentity == null || gameStrategy == null) return;

        System.out.printf("\nWe have a winner: %s has won '%s' winning combination.\n",playerIdentity, getGameStrategyTitle(gameStrategy));

        // Keep track of the winners for the game over report.
        Set<GameStrategy> wins = winners.get(playerIdentity);
        if (wins == null) {
            wins = new HashSet<>();
            winners.put(playerIdentity, wins);
        }
        wins.add(gameStrategy);
    }

    @Override
    public void gameOverEvent() {
        System.out.println("**** Game Over ****\n\n");
        System.out.println("=======================================");
        System.out.println("      Summary:");
        players.forEach(playerIdentity -> {
            StringBuilder winReport = new StringBuilder();
            Set<GameStrategy> wins = winners.get(playerIdentity);
            if (wins != null && !wins.isEmpty()) {
                wins.forEach(win -> {
                    if (winReport.length() > 0) {
                        winReport.append(" and ");
                    }
                    winReport.append(getGameStrategyTitle(win));
                });
            } else {
                winReport.append("Nothing");
            }
            System.out.printf("%s: %s\n", playerIdentity, winReport);
        });
        System.out.println("=======================================");

        System.exit(0);
    }

    String getGameStrategyTitle(final GameStrategy gameStrategy) {
        switch (gameStrategy.getType()) {
            case FULL_HOUSE:
                return"Full House";
            case EARLY_FIVE:
                return "Early Five";
            case TOP_LINE:
                return "Top line";
            default:
                throw new IllegalArgumentException("Unsupported game strategy: " + gameStrategy);
        }
    }

    @Override
    public void matchingNumberEvent(final PlayerIdentity playerIdentity, final BingoNumber bingoNumber) {
        // Ignore - no requirement to broadcast to the world.
    }

    @Override
    public void playerRegisteredEvent(final PlayerIdentity playerIdentity) {
        if (playerIdentity == null) return;

        players.add(playerIdentity);
    }

    @Override
    public void rejectedClaimEvent(PlayerIdentity playerIdentity, GameStrategy gameStrategy, BingoGameCard gameCard, String reason) {
        System.out.println("Claim rejected: " + reason);
    }
}
