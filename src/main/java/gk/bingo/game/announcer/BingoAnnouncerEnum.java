package gk.bingo.game.announcer;

/**
 * Represents the catalog of BingoAnnouncerEnum available in the system.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public enum BingoAnnouncerEnum {
    CONSOLE_ANNOUNCER;
}
