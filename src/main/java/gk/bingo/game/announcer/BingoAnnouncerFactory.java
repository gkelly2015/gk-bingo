package gk.bingo.game.announcer;

import gk.bingo.game.events.MessageBus;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoAnnouncer}. All available concrete implementations are catalogued in the {@link BingoAnnouncerEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoAnnouncerFactory implements AbstractFactory<BingoAnnouncerEnum, BingoAnnouncer> {
    private final MessageBus messageBus;


    public BingoAnnouncerFactory(final MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    @Override
    public BingoAnnouncer create(final BingoAnnouncerEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("BingoAnnouncerEnum cannot be null");

        switch(discriminator) {
            case CONSOLE_ANNOUNCER:
                return new ConsoleBingoAnnouncer(messageBus);
            default:
                throw new IllegalArgumentException("Unsupported BingoAnnouncerEnum type received: " + discriminator);
        }
    }
}
