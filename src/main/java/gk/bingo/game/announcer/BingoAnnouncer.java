package gk.bingo.game.announcer;

import gk.bingo.game.events.*;

/**
 * BingoAnnouncer is responsible for communicating various information related to the game to the world.
 * Since the BingoAnnouncer must be aware of various events in the system, it registers as event listeners for the
 * following:
 * <ul>
 *     <li>{@link PlayerRegisteredListener} - when players register to play the game.</li>
 *     <li>{@link NumberDrawnListener} - when the caller is calling numbers from the bingo machine.</li>
 *     <li>{@link MatchingNumberListener} - when a player matches a bingo number on his bingo game card.</li>
 *     <li>{@link WinningGameCardListener} - when the adjudicator has accepted a winning claim for a bingo game card</li>
 *     <li>{@link GameOverListener} - when the game is over</li>
 *     <li>{@link RejectedClaimListener} - when the adjudicator has rejected a winning claim for a bingo game card</li>
 * </ul>
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoAnnouncer extends PlayerRegisteredListener, NumberDrawnListener, MatchingNumberListener, WinningGameCardListener, RejectedClaimListener, GameOverListener {
}
