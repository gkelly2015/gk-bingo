package gk.bingo.game;

import gk.bingo.game.machine.BingoNumber;

/**
 * The responsibility of a {@link BingoGameCard} is to keep track of the numbers its been assigned, and to
 * answer the question, does this number (provided) appear on your bingo card.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface BingoGameCard {

    /**
     * If the numebr provided is featured on this bingo game card, then return the {@link BingoNumber}
     * representation, which includes the number, and the co-ordinates it appears on the bingo game card.
     * @param number - Integer
     * @return BingoNumber
     */
    BingoNumber checkNumber(final Integer number);

}
