package gk.bingo.game;


import gk.bingo.game.adjudicator.BingoAdjudicator;
import gk.bingo.game.adjudicator.BingoAdjudicatorEnum;
import gk.bingo.game.adjudicator.BingoAdjudicatorFactory;
import gk.bingo.game.announcer.BingoAnnouncer;
import gk.bingo.game.announcer.BingoAnnouncerEnum;
import gk.bingo.game.announcer.BingoAnnouncerFactory;
import gk.bingo.game.caller.BingoCaller;
import gk.bingo.game.caller.BingoCallerEnum;
import gk.bingo.game.caller.BingoCallerFactory;
import gk.bingo.game.events.MessageBus;
import gk.bingo.game.machine.BingoMachine;
import gk.bingo.game.machine.BingoMachineEnum;
import gk.bingo.game.machine.BingoMachineFactory;
import gk.bingo.game.registrar.BingoRegistrar;
import gk.bingo.game.registrar.BingoRegistrarEnum;
import gk.bingo.game.registrar.BingoRegistrarFactory;
import gk.bingo.game.strategies.GameStrategy;
import gk.bingo.game.strategies.GameStrategyEnum;
import gk.bingo.game.strategies.GameStrategyFactory;
import gk.patterns.AbstractFactory;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Builder pattern implementation that allows instances of {@link BingoGame} to be created in a controlled manner.
 * This class may be extended which allows the behavior of the various build methods to be modified as appropriate.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoGameBuilder {

    private final MessageBus messageBus;
    private final BingoGameParameters bingoGameParameters;
    private BingoAnnouncerEnum bingoAnnouncerEnum = BingoAnnouncerEnum.CONSOLE_ANNOUNCER;
    private BingoCallerEnum bingoCallerEnum = BingoCallerEnum.STANDARD_CALLER;
    private BingoAdjudicatorEnum bingoAdjudicatorEnum = BingoAdjudicatorEnum.STANDARD_BINGO_ADJUDICATOR;
    private BingoMachineEnum bingoMachineEnum = BingoMachineEnum.STANDARD_BINGO_MACHINE;
    private BingoRegistrarEnum bingoRegistrarEnum = BingoRegistrarEnum.STANDARD_REGISTRAR;
    private BingoGameCardEnum bingoGameCardEnum = BingoGameCardEnum.OPTIMIZED_BINGO_GAME_CARD;


    public BingoGameBuilder(final MessageBus messageBus, final BingoGameParameters bingoGameParameters) {
        this.messageBus = messageBus;
        this.bingoGameParameters = bingoGameParameters;
    }

    public BingoGameBuilder withBingoAnnouncerEnum(final BingoAnnouncerEnum bingoAnnouncerEnum ) {
        this.bingoAnnouncerEnum = bingoAnnouncerEnum;
        return this;
    }

    public BingoGameBuilder withBingoCallerEnum(final BingoCallerEnum bingoCallerEnum ) {
        this.bingoCallerEnum = bingoCallerEnum;
        return this;
    }

    public BingoGameBuilder withBingoAdjudicatorEnum(final BingoAdjudicatorEnum bingoAdjudicatorEnum ) {
        this.bingoAdjudicatorEnum = bingoAdjudicatorEnum;
        return this;
    }

    public BingoGameBuilder withBingoMachineEnum(final BingoMachineEnum bingoMachineEnum ) {
        this.bingoMachineEnum = bingoMachineEnum;
        return this;
    }

    public BingoGameBuilder withBingoRegistrarEnum(final BingoRegistrarEnum bingoRegistrarEnum ) {
        this.bingoRegistrarEnum = bingoRegistrarEnum;
        return this;
    }

    public BingoGameBuilder withBingoGameCardEnum(final BingoGameCardEnum bingoGameCardEnum ) {
        this.bingoGameCardEnum = bingoGameCardEnum;
        return this;
    }

    protected BingoAnnouncer buildBingoAnnouncer(final MessageBus messageBus) {
        final AbstractFactory<BingoAnnouncerEnum, BingoAnnouncer> factory = new BingoAnnouncerFactory(messageBus);
        return factory.create(bingoAnnouncerEnum);
    }

    protected BingoCaller buildBingoCaller(final MessageBus messageBus, final BingoMachine bingoMachine) {
        final AbstractFactory<BingoCallerEnum, BingoCaller> factory = new BingoCallerFactory(messageBus, bingoMachine);
        return factory.create(bingoCallerEnum);
    }

    protected BingoAdjudicator buildBingoAdjudicator(final MessageBus messageBus, final BingoGameParameters bingoGameParameters) {
        final Map<GameStrategyEnum, GameStrategy> gameStrategies = buildGameStrategies(bingoGameParameters);

        final AbstractFactory<BingoAdjudicatorEnum, BingoAdjudicator> factory = new BingoAdjudicatorFactory(messageBus, gameStrategies);
        return factory.create(bingoAdjudicatorEnum);
    }

    protected Map<GameStrategyEnum, GameStrategy> buildGameStrategies(final BingoGameParameters bingoGameParameters) {
        final AbstractFactory<GameStrategyEnum, GameStrategy> factory = new GameStrategyFactory(bingoGameParameters);
        final Map<GameStrategyEnum, GameStrategy> strategies = new EnumMap<>(GameStrategyEnum.class);
        Stream.of(GameStrategyEnum.values()).forEach(gameStrategyEnum -> {
            strategies.put(gameStrategyEnum, factory.create(gameStrategyEnum));
        });
        return Collections.synchronizedMap(strategies);
    }

    protected BingoMachine buildBingoMachine(final BingoGameParameters bingoGameParameters) {
        final AbstractFactory<BingoMachineEnum, BingoMachine> factory = new BingoMachineFactory(bingoGameParameters);
        return factory.create(bingoMachineEnum);
    }

    protected AbstractFactory<BingoGameCardEnum, BingoGameCard> buildGameCardFactory(final BingoMachine bingoMachine) {
        return new BingoGameCardFactory(bingoMachine);
    }

    protected BingoRegistrar buildBingoRegistrar(final MessageBus messageBus, final BingoGameParameters bingoGameParameters, final BingoMachine bingoMachine) {
        final AbstractFactory<BingoGameCardEnum, BingoGameCard> gameCardFactory = buildGameCardFactory(bingoMachine);
        final AbstractFactory<GameStrategyEnum, GameStrategy> gameStrategyFactory = new GameStrategyFactory(bingoGameParameters);
        final AbstractFactory<BingoRegistrarEnum, BingoRegistrar> factory = new BingoRegistrarFactory(messageBus, bingoGameParameters, gameStrategyFactory, gameCardFactory, bingoGameCardEnum);
        return factory.create(bingoRegistrarEnum);
    }

    public BingoGame build() {
        bingoGameParameters.validate();

        final BingoAnnouncer bingoAnnouncer = buildBingoAnnouncer(messageBus);
        final BingoMachine bingoMachine = buildBingoMachine(bingoGameParameters);
        final BingoAdjudicator bingoAdjudicator = buildBingoAdjudicator(messageBus, bingoGameParameters);
        final BingoCaller bingCaller = buildBingoCaller(messageBus, bingoMachine);
        final BingoRegistrar bingoRegistrar = buildBingoRegistrar(messageBus, bingoGameParameters, bingoMachine);
        return new StandardBingoGame(bingoRegistrar, bingCaller, bingoAnnouncer, bingoAdjudicator);
    }


}
