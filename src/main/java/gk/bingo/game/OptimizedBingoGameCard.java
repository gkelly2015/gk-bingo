package gk.bingo.game;

import gk.bingo.game.machine.BingoNumber;
import gk.datastructure.BinarySearchTree;

import java.util.stream.Stream;

/**
 * Implementtion of a {@link BingoGameCard} that utilizes an instance of a {@link BinarySearchTree} to optimize
 * the retrieval of {@link BingoNumber}'s represented by the game card. The worse-case running time of this
 * implementation is O (log n) which is much better than the exponential running time of a multi-dimensional
 * array based implementation.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
class OptimizedBingoGameCard implements BingoGameCard {

    /**
     * Multi-dimensional array representing the numbers of the game card.
     */
    private final BinarySearchTree<Integer, BingoNumber> numbers;

    /**
     * Constructor. Create a new GameCard.
     * @param numbers list of {@link BingoNumber} that belong to this GameCard.
     */
    OptimizedBingoGameCard(final BingoNumber[] numbers) {
        this.numbers = new BinarySearchTree<>();
        Stream.of(numbers).forEach(n -> this.numbers.put(n.getNumber(), n));
    }

    /**
     * Determine whether the number provided is contained within the numbers
     * datastructure. If so, return the {@link BingoNumber} otherwise return null.
     * @param number Integer representing the number we are checking for.
     * @return BingoNumber the number retrieved from the numbers datastructure.
     */
    public BingoNumber checkNumber(final Integer number) {
        return numbers.get(number);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedBingoGameCard)) return false;

        OptimizedBingoGameCard that = (OptimizedBingoGameCard) o;

        return numbers != null ? numbers.equals(that.numbers) : that.numbers == null;

    }

    @Override
    public int hashCode() {
        return numbers != null ? numbers.hashCode() : 0;
    }
}
