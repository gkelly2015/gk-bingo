package gk.bingo.game;

import gk.bingo.game.machine.BingoMachine;
import gk.patterns.AbstractFactory;

/**
 * Implementation of an {@link AbstractFactory} that is responsible for the creation of concrete implementations
 * of {@link BingoGameCard}. All available concrete implementations are catalogued in the {@link BingoGameCardEnum}.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public class BingoGameCardFactory implements AbstractFactory<BingoGameCardEnum, BingoGameCard> {

    private final BingoMachine bingoMachine;

    public BingoGameCardFactory(final BingoMachine bingoMachine) {
        this.bingoMachine = bingoMachine;
    }

    @Override
    public BingoGameCard create(BingoGameCardEnum discriminator) {
        if (discriminator == null) throw new NullPointerException("GameCardEnum cannot be null");

        switch (discriminator) {
            case OPTIMIZED_BINGO_GAME_CARD:
                return new OptimizedBingoGameCard(bingoMachine.generateNumbers());
            default:
                throw new IllegalArgumentException("Unsupported GameCardEnum received: " + discriminator);
        }
    }
}
