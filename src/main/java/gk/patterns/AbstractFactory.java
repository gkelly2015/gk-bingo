package gk.patterns;

/**
 * Abstract factory design pattern.
 * @param <E> - catalog of instances that are available in the form of an enumeration.
 * @param <T> - the interface of the abstarct types that can be created by this factory.
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface AbstractFactory<E extends Enum<E>, T> {

    /**
     * Based on the discriminator provided, create the concrete instance of the class.
     * @param discriminator - E
     * @return - T
     */
    T create(final E discriminator);
}
