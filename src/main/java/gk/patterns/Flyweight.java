package gk.patterns;

/**
 * Flyweight design pattern.
 * @param <K> - K representing the Key value
 * @param <V> - V representing the value
 *
 * @since 1.0
 * @author Gordon Kelly
 */
public interface Flyweight<K, V> {

    /**
     * Attempt to retrieve the object represented by the key value from the cache.
     * @param key - the key value
     * @return - the value belonging to the key.
     */
    V get(K key);

    /**
     * If no key is found, a new object is created and added to the cache before being
     * handed off to the caller.
     * @param key - the key value.
     * @return - the newly created value.
     */
    V create(K key);

}
